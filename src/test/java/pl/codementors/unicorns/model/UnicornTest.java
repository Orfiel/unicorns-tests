package pl.codementors.unicorns.model;

import org.junit.Assert;
import org.junit.Test;

public class UnicornTest {

    @Test
    public void UnicornWhenNameProvided() {
        String name = "rainbow";
        Unicorn unicorn = new Unicorn(name);
//        assert name.equals(unicorn.getName()) : "name not set";
        Assert.assertEquals("name not set", name, unicorn.getName());
    }

    @Test
    public void UnicornWhenNameNotProvided() {
        Unicorn unicorn = new Unicorn();
        Assert.assertNull(unicorn.getName());
    }

    @Test
    public void setName() {
        Unicorn unicorn = new Unicorn();
        unicorn.setName("rainbow");
        Assert.assertEquals("name not set properly","rainbow", unicorn.getName());
    }

    @Test
    public void equalsWhenTheSameReference() {
        Unicorn u1 = new Unicorn();
        Unicorn u2 = u1;
        boolean ret = u1.equals(u2);
        Assert.assertTrue("objects should be the same", ret);
    }

    @Test
    public void equalsWhenDifferentReferenceSameValue() {
        Unicorn u1 = new Unicorn(new String("rainbow"));
        Unicorn u2 = new Unicorn(new String("rainbow"));
        boolean ret = u1.equals(u2);
        Assert.assertTrue("objects should be the same", ret);
    }

    @Test
    public void equalsWHenDifferentReferenceDifferentValue() {
        Unicorn u1 = new Unicorn(new String("rainbow"));
        Unicorn u2 = new Unicorn(new String("pinky"));
        boolean ret = u1.equals(u2);
        Assert.assertFalse("objects should not be the same", ret);
    }

    @Test
    public void hashCodeWhenSameValue() {
        Unicorn u1 = new Unicorn(new String("rainbow"));
        Unicorn u2 = new Unicorn(new String("rainbow"));
        int hash1 = u1.hashCode();
        int hash2 = u2.hashCode();
        Assert.assertTrue("hash codes should be the same", hash1 == hash2);
    }

    @Test
    public void toStringWhenNameSet() {
        Unicorn u1 = new Unicorn("applejack");
        String toString = u1.toString();
        Assert.assertEquals("string wrong formatter", "Unicorn{name='applejack'}", toString);
    }
}
